# About the datasets

## **If you use these datasets please cite:**

- Bonami, Olivier, and Juliette Thuilier. "A statistical approach to affix rivalry: French -iser and -ifier." Word Structure 12, no 1 (2019), 4-41.

- Guzmán Naranjo, M. (2019). *Analogical classification in formal grammar*. Language Science Press. Series: Empirically Oriented Theoretical Morphology and Syntax 


